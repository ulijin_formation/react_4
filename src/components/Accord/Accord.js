import React, { useState, useRef, useEffect, useLayoutEffect } from 'react'
import './Accord.css'
import Chevron from './chevron.svg'
export default function Accord() {

  const [toggle, setToggle] = useState(false);
  const [heightEl, setHeightEl] = useState();
  const toggleState = () => {
    setToggle(!toggle);

  }
  const refHeight = useRef();
  useEffect(() => {
    setHeightEl(`${refHeight.current.scrollHeight}px`)
  }, [])



  return (
    <div className='accord'>

      <div
        onClick={toggleState}
        className='accord-visible'>
        <h2>Ceci est un Accordéon</h2>
        <img src={Chevron} alt="chevron-down" />
      </div>
      <div ref={refHeight}
        className={toggle ? 'accord-toggle animated' : 'accord-toggle'}>
        <p
          aria-hidden={toggle ? "true" : "false"}
        >
          Quand on clique sur le chevron, j'apparais!
        </p>

      </div>

    </div>
  )
}
